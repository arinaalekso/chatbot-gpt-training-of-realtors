<?
include 'index.php';
include 'main_auto.php';
include 'info_emp.php';
?>
<html>
    <head>
        <meta charset='utf-8'> 
        <link rel="stylesheet" href="../css/pers.css">
        <link rel="icon" href="../img/logo.ico" type="image/x-icon">
        <title>Обучение риелторов</title>
    </head>
    <body>
        <form method="POST" action="">  
            <header>
                <img id="logo_foto" src="../img/logo.png">
                    <div class="heads_menu">
                        <nav>
                            <ul class = "menu">
                                <li > <!-- id="li_go" -->
                                    <a href="personal_rating.php">Личный рейтинг</a>
                                </li>
                                <li>
                                    <a href="info_about_dep.php">Информация об отделе </a>
                                </li>
                                <li>
                                    <a href="overall_rating.php">Общий рейтинг</a>
                                </li>                    
                            </ul>
                        </nav>        
                    </div>
                    <div class="acc" >
                        <button class="dropbtn_acc"> 
                            <div  class="img_ac">
                                <img class="li_go" src="../img/no_foto.png">
                            </div>
                            <div class="arrow"> </div>  
                        </button>     
                        <div class="dropdown-content">
                            <a href="pers.php">
                                <div class="menu_drop_img_1">
                                    <img src="../img/no_foto.png">
                                </div>
                                <div class="menu_drop_1">
                                    <span><? echo $surname_value.' '.$name_value ?></span> 
                                </div>
                            </a>
                            <a href="setting_pers.php">
                                <div class="menu_drop_img_2">
                                    <img src="../img/exit.png">
                                </div>
                                <div class="menu_drop_2">
                                    <span>
                                        Настройки
                                    </span>
                                </div>
                            </a> 
                                <div class="menu_drop_img_3">
                                    <img src="../img/setting.png">
                                </div>
                                <div class="menu_drop_3">
                                    <input type="submit" value="Выход" name="log_on" >
                                </div>
                             
                        </div>    
                    </div>
            </header>
            <div class="reg_form">
                <h1>Личный кабинет</h1> 
                <div class="info"> 
                    <fieldset class="perinfo">
                        <legend>Личная информация</legend>
                        <div class="perinfo_1">
                            <div class="perinfo_left">
                                <div class="left_p">
                                    <p id="text_label">Фамилия</p>
                                    <p id="text_label">Имя</p>
                                    <p id="text_label">Отчество</p>
                                </div>
                                <div class="right_p">
                                    <input type="text" name="surname" value="<?php print_r ($surname_value);?>"  >
                                    <p class="com1"></p>
                                    <input type="text" name="name" value="<?php print_r ($name_value);?>"  >
                                    <p class="com1"></p>
                                    <input type="text" name="patronymic" value="<?php print_r ($patronymic_value);?>"  >
                                    <p class="com1"></p>
                                </div>
                            </div>
                            <div class="perinfo_right">
                                <div class="left_p1">
                                    <p id="text_label">Дата рождения</p>
                                    <p id="text_label">Номер телефона</p>
                                    <p id="text_label">Эл. Почта</p>
                                </div>
                                <div class="right_p1">
                                    <input type="date" name="dr" value="<?php print_r ($dr_value);?>"  >
                                    <p class="com1"></p>
                                    <input type="text" name="phonenumber" value="<?php print_r ($phonenumber_value);?>"  >
                                    <p class="com1"></p>
                                    <input type="text" name="email" value="<?php print_r ($email_value);?>"  >
                                    <p class="com1"></p>
                                </div>
                            </div>
                        </div>
                    </fieldset>    
                    <fieldset class="workinfo">
                        <legend>Информация о работе</legend> 
                        <div class="work_info">
                            <div class="work_info_left">  
                                <p id="text_label">Отдел</p> 
                                <select name="id_dep" placeholder="<?php print_r ($city_department);?>"> 
                                <?php
                                    $query2 ="SELECT id, city FROM department";
                                    $result_dep = $conn->query($query2);
                                    while($row = $result_dep->fetch())
                                    {
                                        echo "<option value=".$row['id'].">".$row['city']."</option>";
                                    } 
                                ?> 
                                </select>
                                
                                <p class="com1"></p>
                            </div>
                            <div class="work_info_right"> 
                                <p id="text_label">Должность</p> 
                                <input type="text" name="post" value="<?php print_r ($post_value);?>"> 
                                <p class="com1"></p>
                            </div>  
                        </div>          
                    </fieldset>
                    <fieldset class="teleinfo">
                        <legend>Логин в Telegram</legend>
                        <div  class="tele_info">
                            <img src="../img/icons8-telegram-app-36.png">
                            <input type="text" name="tele_login" value="<?php
                            if ($result_telelogin->rowCount()>0) 
                            {
                                print_r ($telelogin);
                            }
                            ?>">
                            <p class="com1"></p>
                        </div> 
                    </fieldset>
                    <fieldset class="exitinfo">
                        <legend>Данные входа</legend>
                        <div class="exit_info">
                            <div class="exit_info_left"> 
                                <p id="text_label">Логин</p> 
                                <input type="text" name="login" value="<?php print_r ($login_value);?>">
                                <p class="com1"></p>
                            </div>
                            <div class="exit_info_right"> 
                                <p id="text_label">Пароль</p> 
                                <input type="text" name="password"  value="<?php print_r ($pass_value);?>"> 
                                <p class="com2"></p>
                            </div>  
                        </div>  
                    </fieldset>
                    <div class="btn">
                        <input type="submit" value="Готово" name="go">
                    </div>
                </div> 
        </div>
        </form>
        <footer class="footer_setpers">
            <div class="footer_h">
                <img id="logo_f" src="../img/logo.png">
                <h4>ОБУЧЕНИЕ&nbspРИЕЛТОРОВ</h4>
            </div>
            
            <div class="teleg">
                <a href="https://t.me/trainingrealtors_bot" title="ЧАТ-БОТ'Обучение Риэлторов'" target="_blank">
                    <img src="../img/Telegram.png"> ЧАТ-БОТ “обучение риелторов”
                </a>
            </div>
            <p> &copy;	2024 ”ОБУЧЕНИЕ РИЕЛТОРОВ” Made by @ar.alekso</p>
        </footer>  <!---->
    </body>
</html>