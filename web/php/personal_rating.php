<?
include 'index.php';
include 'main_auto.php';
include 'info_emp.php';
?>
<html>
    <head>
        <meta charset='utf-8'> 
        <link rel="stylesheet" href="../css/personal_rating.css">
        <link rel="icon" href="../img/logo.ico" type="image/x-icon"> 
        <title>Обучение риелторов</title>  
    </head>
    <body>
        <form method="POST" action="" >
            <header>
                <img id="logo_foto" src="../img/logo.png">
                    <div class="heads_menu">
                        <nav>
                            <ul class = "menu">
                                <li class="li_go" >
                                    <a id="li_go" href="personal_rating.php">Личный рейтинг</a>
                                </li>
                                <li>
                                    <a href="info_about_dep.php">Информация об отделе </a>
                                </li>
                                <li>
                                    <a href="overall_rating.php">Общий рейтинг</a>
                                </li>                    
                            </ul>
                        </nav>        
                    </div>
                    <div class="acc" >
                        <button class="dropbtn_acc"> 
                            <div class="img_ac">
                                <img src="../img/no_foto.png">
                            </div>
                            <div class="arrow"> </div>  
                        </button>     
                        <div class="dropdown-content">
                            <a href="pers.php">
                                <div class="menu_drop_img_1">
                                    <img src="../img/no_foto.png">
                                </div>
                                <div class="menu_drop_1">
                                    <span><? echo $surname_value.' '.$name_value ?></span>  
                                </div>
                            </a>
                            <a href="setting_pers.php">
                                <div class="menu_drop_img_2">
                                    <img src="../img/exit.png">
                                </div>
                                <div class="menu_drop_2">
                                    <span>
                                        Настройки
                                    </span>
                                </div>
                            </a>  
                            <div class="menu_drop_img_3">
                                <img src="../img/setting.png">
                            </div>
                            <div class="menu_drop_3">
                                 
                                    <input type="submit" value="Выход" name="log_on" > 
                            </div> 
                        </div>    
                    </div>
            </header>
            <main>
                <?php
                    $sql_point = "SELECT point FROM pointstraining WHERE id_emp='$id_value'";
                    $result_point = $conn->query($sql_point);
                    while($row = $result_point->fetch())
                    {
                        $point=$row['point'];
                    }

                ?>
                <h1>Личный рейтинг</h1>
                <div class="raiting">
                    <select name="sel_time" id="">
                        <option value="week">Неделя</option>
                        <option value="mounth">Месяц</option>
                        <option value="years">Год</option>
                    </select>
                    <input type="submit" name="see" value="Показать">
                    <div class="total_points">
                        <h2>Общие баллы</h2>
                        <p>Ваши общее количество баллов за все время: <?php echo $point;?></p>
                        <div class="histogram_points" style='height:auto; padding:10px'>
                            <?php 
                                if (!empty($_POST['see'])) 
                                {
                                    if ($_POST['sel_time']=="week") 
                                    {
                                        $currentDate = date('Y-m-d');
                                        $startOfWeek = date('Y-m-d', strtotime("last monday", strtotime($currentDate))); 
                                        $endOfWeek = date('Y-m-d', strtotime("next sunday", strtotime($currentDate)));
                                        $arr_date=[];
                                        $arr_points=[];
                                        $arr_ph= array();
                                        $sql_p = "SELECT * FROM time_and_points WHERE id_emp='$id_value' and times BETWEEN '$startOfWeek' AND '$endOfWeek' ORDER BY id ";
                                        $result_p= $conn->query($sql_p);  
                                        while($row = $result_p->fetch())
                                        {
                                            array_push($arr_date,$row['times']);
                                            array_push($arr_points,$row['points']); 
                                        }     
                                        echo "<p>Ваши баллы за эту неделю: ".array_sum ($arr_points)." </p>";
                                        for ($i = 0; $i < count($arr_date); $i++) 
                                        { 
                                            if (!isset($arr_ph[$arr_date[$i]])) 
                                            { 
                                                $arr_ph[$arr_date[$i]] = $arr_points[$i];
                                            } 
                                            else 
                                            { 
                                                $arr_ph[$arr_date[$i]] += $arr_points[$i];
                                            }
                                        }  
                                        foreach($arr_ph as $key => $ph) 
                                        {
                                            echo "<div class='row' title='".date('d-m-Y',strtotime($key))."' style=' height:".($ph*5)."px'><p style=' color:black'>".$ph." б. </p></div>";
                                        }
                                    }
                                    if ($_POST['sel_time']=="mounth") 
                                    {
                                        $currentDate = date('Y-m-d');
                                        $firstDayOfMonth = date('Y-m-d', strtotime("first day of this month", strtotime($currentDate))); 
                                        $lastDayOfMonth = date('Y-m-d', strtotime("last day of this month", strtotime($currentDate)));
                                        $arr_date=[];
                                        $arr_points=[];
                                        $arr_ph= array();
                                        $sql_p = "SELECT * FROM time_and_points WHERE id_emp='$id_value' and times BETWEEN '$firstDayOfMonth' AND '$lastDayOfMonth' ORDER BY id ";
                                        $result_p= $conn->query($sql_p); 
                                        
                                        while($row = $result_p->fetch())
                                        {
                                            array_push($arr_date,$row['times']);
                                            array_push($arr_points,$row['points']); 
                                        }      
                                        for ($i = 0; $i < count($arr_date); $i++) 
                                        { 
                                            if (!isset($arr_ph[$arr_date[$i]])) 
                                            { 
                                                $arr_ph[$arr_date[$i]] = $arr_points[$i];
                                            } 
                                            else 
                                            { 
                                                $arr_ph[$arr_date[$i]] += $arr_points[$i];
                                            }
                                        }  
                                        echo "<p>Ваши баллы за этот месяц: ".array_sum ($arr_points)." </p>";
                                        foreach($arr_ph as $key => $ph) 
                                        {
                                            echo "<div class='row' title='".date('d-m-Y',strtotime($key))."' style=' height:".($ph*5)."px'><p style=' color:black'>".$ph." б. </p></div>";
                                        }
                                    }
                                    if ($_POST['sel_time']=="years") 
                                    {
                                        $currentDate = date('Y-m-d');
                                        $firstDayOfYear = date('Y-m-d', strtotime("first day of january this year", strtotime($currentDate))); 
                                        $lastDayOfYear = date('Y-m-d', strtotime("last day of December this year", strtotime($currentDate)));
                                        $arr_date=[];
                                        $arr_points=[];
                                        $arr_ph= array();
                                        $sql_p = "SELECT * FROM time_and_points WHERE id_emp='$id_value' and times BETWEEN '$firstDayOfYear' AND '$lastDayOfYear' ORDER BY id ";
                                        $result_p= $conn->query($sql_p);  
                                        while($row = $result_p->fetch())
                                        {
                                            array_push($arr_date,$row['times']);
                                            array_push($arr_points,$row['points']); 
                                        }     
                                        echo "<p>Ваши баллы за этот год: ".array_sum ($arr_points)." </p>";
                                        for ($i = 0; $i < count($arr_date); $i++) 
                                        { 
                                            if (!isset($arr_ph[$arr_date[$i]])) 
                                            { 
                                                $arr_ph[$arr_date[$i]] = $arr_points[$i];
                                            } 
                                            else 
                                            { 
                                                $arr_ph[$arr_date[$i]] += $arr_points[$i];
                                            }
                                        }  
                                        foreach($arr_ph as $key => $ph) 
                                        {
                                            echo "<div class='row' title='".date('d-m-Y',strtotime($key))."' style=' height:".($ph*5)."px'><p style=' color:black'>".$ph." б. </p></div>";
                                        }
                                    }
                                    
                                }
                                else
                                {
                                    $currentDate = date('Y-m-d');
                                    $startDate = date('Y-m-d', strtotime("-7 days", strtotime($currentDate)));
                                    $arr_date=[];
                                    $arr_points=[];
                                    $arr_ph= array();
                                    $sql_p = "SELECT * FROM time_and_points WHERE id_emp='$id_value' and times BETWEEN '$startDate' AND '$currentDate' ORDER BY id ";
                                    $result_p= $conn->query($sql_p);  
                                    while($row = $result_p->fetch())
                                    {
                                        array_push($arr_date,$row['times']);
                                        array_push($arr_points,$row['points']); 
                                    }     
                                    echo "<p>Ваши баллы за последнии 7 дней: ".array_sum ($arr_points)." </p>";
                                    for ($i = 0; $i < count($arr_date); $i++) 
                                    { 
                                        if (!isset($arr_ph[$arr_date[$i]])) 
                                        { 
                                            $arr_ph[$arr_date[$i]] = $arr_points[$i];
                                        } 
                                        else 
                                        { 
                                            $arr_ph[$arr_date[$i]] += $arr_points[$i];
                                        }
                                    }  
                                    foreach($arr_ph as $key => $ph) 
                                    {
                                        echo "<div class='row' title='".date('d-m-Y',strtotime($key))."' style=' height:".($ph*5)."px'><p style=' color:black'>".$ph." б. </p></div>";
                                    }
                                }
                            ?>
                        </div>
                    </div>
                    <!----> <div class="pace">
                        <h2>Скорость</h2>
                        <div class="histogram_pace"  style='height:auto; padding:10px'>
                        <?php 
                                if (!empty($_POST['see'])) 
                                {
                                    if ($_POST['sel_time']=="week") 
                                    {
                                        $currentDate = date('Y-m-d');
                                        $startOfWeek = date('Y-m-d', strtotime("last monday", strtotime($currentDate))); 
                                        $endOfWeek = date('Y-m-d', strtotime("next sunday", strtotime($currentDate))); 
                                        $arr_points=[];
                                        $arr_ph= array();
                                        $sql_p = "SELECT * FROM info_dialog WHERE id_emp='$id_value' and date_mess BETWEEN '$startOfWeek' AND '$endOfWeek' ORDER BY id ";
                                        $result_p= $conn->query($sql_p);  
                                        while($row = $result_p->fetch())
                                        {
                                            array_push($arr_points, substr($row['totaltime'],0,8)); 
                                        }      
                                        echo "<p style=' font-size:25px'>Диаграмма скорости за эту неделю:  </p>";
                                        for ($i = 0; $i < count($arr_points); $i++) 
                                        { 
                                            $arr_ph [$i]= substr($arr_points[$i],3,2); 
                                        }   
                                        foreach($arr_ph as $key => $ph) 
                                        { 
                                            echo "<div class='row' title='".date('d-m-Y',strtotime($key))."' style=' height:".($ph*15)."px'><p style=' color:black'>".$ph." м. </p></div>";
                                        }
                                    }
                                    if ($_POST['sel_time']=="mounth") 
                                    {
                                        $currentDate = date('Y-m-d');
                                        $firstDayOfMonth = date('Y-m-d', strtotime("first day of this month", strtotime($currentDate))); 
                                        $lastDayOfMonth = date('Y-m-d', strtotime("last day of this month", strtotime($currentDate))); 
                                        $arr_points=[];
                                        $arr_ph= array();
                                        $sql_p = "SELECT * FROM info_dialog WHERE id_emp='$id_value' and date_mess BETWEEN '$firstDayOfMonth' AND '$lastDayOfMonth' ORDER BY id ";
                                        $result_p= $conn->query($sql_p);  
                                        while($row = $result_p->fetch())
                                        {
                                            array_push($arr_points, substr($row['totaltime'],0,8)); 
                                        }      
                                        echo "<p style=' font-size:25px'>Диаграмма скорости за этот месяц: </p>";
                                        for ($i = 0; $i < count($arr_points); $i++) 
                                        { 
                                            $arr_ph [$i]= substr($arr_points[$i],3,2); 
                                        }   
                                        foreach($arr_ph as $key => $ph) 
                                        { 
                                            echo "<div class='row' title='".date('d-m-Y',strtotime($key))."' style=' height:".($ph*15)."px'><p style=' color:black'>".$ph." м. </p></div>";
                                        }
                                    }
                                    if ($_POST['sel_time']=="years") 
                                    {
                                        $currentDate = date('Y-m-d');
                                        $firstDayOfYear = date('Y-m-d', strtotime("first day of january this year", strtotime($currentDate))); 
                                        $lastDayOfYear = date('Y-m-d', strtotime("last day of December this year", strtotime($currentDate)));
                                        $arr_date=[];
                                        $arr_points=[];
                                        $arr_ph= array();
                                        $sql_p = "SELECT * FROM info_dialog WHERE id_emp='$id_value' and date_mess BETWEEN '$firstDayOfYear' AND '$lastDayOfYear' ORDER BY id ";
                                        $result_p= $conn->query($sql_p);  
                                        while($row = $result_p->fetch())
                                        {
                                            array_push($arr_points, substr($row['totaltime'],0,8)); 
                                        }      
                                        echo "<p style=' font-size:25px'>Диаграмма скорости за этот год:  </p>";
                                        for ($i = 0; $i < count($arr_points); $i++) 
                                        { 
                                            $arr_ph [$i]= substr($arr_points[$i],3,2); 
                                        }   
                                        foreach($arr_ph as $key => $ph) 
                                        { 
                                            if ($ph!="00")
                                            {
                                                echo "<div class='row' title='".date('d-m-Y',strtotime($key))."' style=' height:".($ph*15)."px'><p style=' color:black'>".$ph." м. </p></div>";
                                            }
                                        }
                                    }
                                    
                                }
                                else
                                {
                                    $currentDate = date('Y-m-d');
                                    $startDate = date('Y-m-d', strtotime("-7 days", strtotime($currentDate)));
                                    $arr_date=[];
                                    $arr_points=[];
                                    $arr_ph= array();
                                    $sql_p = "SELECT * FROM time_and_points WHERE id_emp='$id_value' and times BETWEEN '$startDate' AND '$currentDate' ORDER BY id ";
                                    $result_p= $conn->query($sql_p);  
                                    while($row = $result_p->fetch())
                                    {
                                        array_push($arr_date,$row['times']);
                                        array_push($arr_points,$row['points']); 
                                    }     
                                    echo "<p style=' font-size:25px'>Диаграмма скорости за последнии 7 дней:</p>";
                                    for ($i = 0; $i < count($arr_date); $i++) 
                                    { 
                                        if (!isset($arr_ph[$arr_date[$i]])) 
                                        { 
                                            $arr_ph[$arr_date[$i]] = $arr_points[$i];
                                        } 
                                        else 
                                        { 
                                            $arr_ph[$arr_date[$i]] += $arr_points[$i];
                                        }
                                    }  
                                    foreach($arr_ph as $key => $ph) 
                                    {
                                        echo "<div class='row' title='".date('d-m-Y',strtotime($key))."' style=' height:".($ph*5)."px'><p>".$ph." б. </p></div>";
                                    }
                                }
                                ?>
                        </div>
                    </div>  
                </div>
            </main>
        </form>
        <footer>
            <div class="footer_h">
                <img id="logo_f" src="../img/logo.png">
                <h4>ОБУЧЕНИЕ&nbspРИЕЛТОРОВ</h4>
            </div>
            
            <div class="teleg">
                <a href="https://t.me/trainingrealtors_bot" title="ЧАТ-БОТ'Обучение Риэлторов'" target="_blank">
                    <img src="../img/Telegram.png"> ЧАТ-БОТ “обучение риелторов”
                </a>
            </div>
            <p> &copy;	2024 ”ОБУЧЕНИЕ РИЕЛТОРОВ” Made by @ar.alekso</p>
        </footer>
    </body>
</html>