<?
include 'index.php';
include 'main_auto.php';
include 'info_reg.php';
?>
<html>
    <head>
        <meta charset='utf-8'> 
        <link rel="stylesheet" href="../css/reg.css">
        <link rel="icon" href="../img/logo.ico" type="image/x-icon">
        <title>Обучение риелторов</title>
    </head>
    <body>
        <header> 
            <img alt="" src="../img/logo.png">
                <div class="heads_menu">
                    <h1>ОБУЧЕНИЕ&nbspРИЕЛТОРОВ</h1>
                </div>
        </header>
        <div class="reg_form">
            <h1>РЕГИСТРАЦИЯ</h1>
            <form method="POST" action="">            
                <div class="info"> 
                    <fieldset class="perinfo">
                        <legend>Личная информация</legend>
                        <div class="perinfo_1">
                            <div class="perinfo_left">
                                <div class="left_p">
                                    <p id="text_label">Фамилия</p>
                                    <p id="text_label">Имя</p>
                                    <p id="text_label">Отчество</p>
                                </div>
                                <div class="right_p">
                                    <input type="text" name="surname">
                                    <p class="com1"></p>
                                    <input type="text" name="name">
                                    <p class="com1"></p>
                                    <input type="text" name="patronymic">
                                    <p class="com1"></p>
                                </div>
                            </div>
                            <div class="perinfo_right">
                                <div class="left_p1">
                                    <p id="text_label">Дата рождения</p>
                                    <p id="text_label">Номер телефона</p>
                                    <p id="text_label">Эл. Почта</p>
                                </div>
                                <div class="right_p1">
                                    <input type="date" name="dr">
                                    <p class="com1"></p>
                                    <input type="text" name="phonenumber">
                                    <p class="com1"></p>
                                    <input type="email" name="email">
                                    <p class="com1"></p>
                                </div>
                            </div>
                        </div>
                    </fieldset>    
                    <fieldset class="workinfo">
                        <legend>Информация о работе</legend> 
                        <div class="work_info">
                            <div class="work_info_left"> 
                                <p id="text_label">Отдел</p> 
                                <select name="id_dep" placeholder="<?php print_r ($city_department);?>"> 
                                <?php
                                    $query2 ="SELECT id, city FROM department";
                                    $result_dep = $conn->query($query2);
                                    while($row = $result_dep->fetch())
                                    {
                                        echo "<option value=".$row['id'].">".$row['city']."</option>";
                                    } 
                                ?> 
                                </select>
                                <p class="com1"></p>
                            </div>
                            <div class="work_info_right"> 
                                <p id="text_label">Должность</p> 
                                <input type="text" name="post"> 
                                <p class="com1"></p>
                            </div>  
                        </div>          
                    </fieldset>
                    <fieldset class="exitinfo">
                        <legend>Данные входа</legend>
                        <div class="exit_info">
                            <div class="exit_info_left"> 
                                <p id="text_label">Логин</p> 
                                <input type="text" name="login">
                                <p class="com1"></p>
                            </div>
                            <div class="exit_info_right"> 
                                <p id="text_label">Пароль</p> 
                                <input type="text" name="password"> 
                                <p class="com2"></p>
                            </div>  
                        </div>  
                    </fieldset>
                    <div class="btn">
                        <input type="submit" value="Зарегистрироваться" name="reges">
                    </div>
                </div>
            </form>
        </div>
        <footer>
            <div class="footer_h">
                <img id="logo_f" src="../img/logo.png">
                <h4>ОБУЧЕНИЕ&nbspРИЕЛТОРОВ</h4>
            </div> 
            <div class="teleg">
                <a href="https://t.me/trainingrealtors_bot" title="ЧАТ-БОТ'Обучение Риэлторов'" target="_blank">
                    <img src="../img/Telegram.png"> ЧАТ-БОТ “обучение риелторов”
                </a>
            </div>
            <p> &copy;	2024 ”ОБУЧЕНИЕ РИЕЛТОРОВ” Made by @ar.alekso</p>
        </footer>
    </body>
</html>