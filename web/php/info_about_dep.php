<?
include 'index.php';
include 'main_auto.php';
include 'info_emp.php';
?>
<html>
    <head>
        <meta charset='utf-8'> 
        <link rel="stylesheet" href="../css/personal_rating.css">
        <link rel="icon" href="../img/logo.ico" type="image/x-icon"> 
        <title>Обучение риелторов</title> 
    </head>
    <body>
        <header>
            <img id="logo_foto" src="../img/logo.png">
                <div class="heads_menu">
                    <nav>
                        <ul class = "menu">
                            <li>
                                <a  href="personal_rating.php">Личный рейтинг</a>
                            </li>
                            <li class="li_go">
                                <a id="li_go"  href="info_about_dep.php">Информация об отделе </a>
                            </li>
                            <li  >
                                <a href="overall_rating.php">Общий рейтинг</a>
                            </li>                    
                        </ul>
                    </nav>        
                </div>
                <div class="acc" >
                    <button class="dropbtn_acc"> 
                        <div class="img_ac">
                            <img src="../img/no_foto.png">
                        </div>
                        <div class="arrow"> </div>  
                    </button>     
                    <div class="dropdown-content">
                        <a href="pers.html">
                            <div class="menu_drop_img_1">
                                <img src="../img/no_foto.png">
                            </div>
                            <div class="menu_drop_1">
                                <span><? echo $surname_value.' '.$name_value ?></span> 
                            </div>
                        </a>
                        <a href="setting_pers.php">
                            <div class="menu_drop_img_2">
                                <img src="../img/exit.png">
                            </div>
                            <div class="menu_drop_2">
                                <span>
                                    Настройки
                                </span>
                            </div>
                        </a> 
                        <a href="../html/main.html">
                            <div class="menu_drop_img_3">
                                <img src="../img/setting.png">
                            </div>
                            <div class="menu_drop_3">
                                <form method="POST" action="">
                                    <input type="submit" value="Выход" name="log_on" >
                                </form>
                            </div>
                        </a> 
                    </div>    
                </div>
        </header>
        <main>
            <?php
                $sql_search_dep = "SELECT * FROM department WHERE id='$dep_value'";
                $result_search_dep = $conn->query($sql_search_dep);
                while($row = $result_search_dep->fetch())
                {
                    $id_d=$row['id'];
                    $city_d=$row['city'];
                    $email_d=$row['email'];
                    $ph_d=$row['phone'];
                }
            ?>
            <h1>Информация об отделе</h1>
            <div class="info_deps">
                <p><b>Город</b> <? echo $city_d;?></p>
                <p><b>Номер Телефона</b> <? echo $ph_d;?></p>
                <p><b>Эл. Почта</b> <? echo $email_d;?></p>
                <div class="table_emp">
                    <h2>Сотрудники отдела г. <? echo $city_d;?> </h2>
                    <?php 
                        $sql_emp_dep = "SELECT * FROM employees WHERE id_dep='$dep_value' ORDER BY id ";
                        $result_emp_dep = $conn->query($sql_emp_dep);
                        if ($result_emp_dep) 
                        {
                            echo 
                            "<table>
                                <tr>
                                    <th>ID</th>
                                    <th>Фамилия</th>
                                    <th>Имя</th>
                                    <th>Отчество</th>
                                    <th>Дата рождения</th>
                                    <th>Номер телефона</th>
                                    <th>Эл. почта</th>
                                    <th>Должность</th> 
                                </tr>";
                            while($row = $result_emp_dep->fetch())
                            {
                                echo "<tr>";
                                for ($j = 0 ; $j <= 10 ; $j++) 
                                { 
                                    if ($j<=6 || $j>=10) 
                                    {
                                        echo "<td>$row[$j]</td>";
                                    }
                                }
                                echo "</tr>"; 
                            }
                            echo "</table>";
                        }    
                    ?>
                </div>
            </div>
        </main>
        <footer>
            <div class="footer_h">
                <img id="logo_f" src="../img/logo.png">
                <h4>ОБУЧЕНИЕ&nbspРИЕЛТОРОВ</h4>
            </div> 
            <div class="teleg">
                <a href="https://t.me/trainingrealtors_bot" title="ЧАТ-БОТ'Обучение Риэлторов'" target="_blank">
                    <img src="../img/Telegram.png"> ЧАТ-БОТ “обучение риелторов”
                </a>
            </div>
            <p> &copy;	2024 ”ОБУЧЕНИЕ РИЕЛТОРОВ” Made by @ar.alekso</p>
        </footer>
    </body>
</html>