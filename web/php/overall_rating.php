<?
include 'index.php';
include 'main_auto.php';
include 'info_emp.php';
?>
<html>
    <head>
        <meta charset='utf-8'> 
        <link rel="stylesheet" href="../css/overall_rating.css">
        <link rel="icon" href="../img/logo.ico" type="image/x-icon"> 
        <title>Обучение риелторов</title> 
    </head>
    <body>
        <form method="POST" action="">
            <header>
                <img id="logo_foto" src="../img/logo.png">
                    <div class="heads_menu">
                        <nav>
                            <ul class = "menu">
                                <li>
                                    <a href="personal_rating.php">Личный рейтинг</a>
                                </li>
                                <li>
                                    <a href="info_about_dep.php">Информация об отделе </a>
                                </li>
                                <li class="li_go" >
                                    <a id="li_go" href="overall_rating.php">Общий рейтинг</a>
                                </li>                    
                            </ul>
                        </nav>        
                    </div>
                    <div class="acc" >
                        <button class="dropbtn_acc"> 
                            <div class="img_ac">
                                <img src="../img/no_foto.png">
                            </div>
                            <div class="arrow"> </div>  
                        </button>     
                        <div class="dropdown-content">
                            <a href="pers.php">
                                <div class="menu_drop_img_1">
                                    <img src="../img/no_foto.png">
                                </div>
                                <div class="menu_drop_1">
                                    <span><? echo $surname_value.' '.$name_value ?></span> 
                                </div>
                            </a>
                            <a href="setting_pers.php">
                                <div class="menu_drop_img_2">
                                    <img src="../img/exit.png">
                                </div>
                                <div class="menu_drop_2">
                                    <span>
                                        Настройки
                                    </span>
                                </div>
                            </a>  
                                <div class="menu_drop_img_3">
                                    <img src="../img/setting.png">
                                </div>
                                <div class="menu_drop_3">
                                    <input type="submit" value="Выход" name="log_on" >
                                </div> 
                        </div>    
                    </div>
            </header>
            <main>
                <h1>Общий рейтинг</h1>
                <div class="raiting">
                    <select name="specifications" id="">
                        <option value="total">Общее количество баллов</option>
                        <option value="speed">Скорость достижения результата</option>
                    </select>
                    <input type="submit" value="Показать">
                    <div class="info_raiting">
                        <h2>Общее количество баллов</h2>
                        <div class="table_raiting">
                        <?php
                            $sql_search_dep = "SELECT * FROM department WHERE id='$dep_value'";
                            $result_search_dep = $conn->query($sql_search_dep);
                            while($row = $result_search_dep->fetch())
                            {
                                $id_d=$row['id'];
                                $city_d=$row['city'];
                                $email_d=$row['email'];
                                $ph_d=$row['phone'];
                            }
                            $num=0;
                            
                            $sql_emp_point = "SELECT * FROM  pointstraining ORDER BY point DESC";
                            $result_emp_point = $conn->query($sql_emp_point);
                            if ($result_emp_point) 
                            {
                                echo 
                                "<table>
                                    <tr> 
                                        <th>№</th>
                                        <th>Ф.И.О. сотрудника</th> 
                                        <th>Баллы</th> 
                                    </tr>";
                                while($row = $result_emp_point->fetch())
                                {
                                    $num++;
                                    echo "<tr>";
                                    echo "<td>$num</td>";
                                    for ($j = 1 ; $j <= 2 ; $j++) 
                                    {  
                                        if ($j==1) 
                                        {
                                            $sql_fio = "SELECT surname, name,patronymic FROM  employees WHERE id='$row[$j]'";
                                            $result_fio = $conn->query($sql_fio);
                                            while($rows = $result_fio->fetch())
                                            {
                                                echo "<td>".$rows['surname']." ".$rows['name']." ".$rows['patronymic']."</td>";
                                            }
                                        } 
                                        else
                                        {
                                            echo "<td>$row[$j]</td>";
                                        } 
                                    }
                                    echo "</tr>"; 
                                }
                                echo "</table>";
                            }    
                        ?> 
                        </div>
                    </div>
                </div>
            </main>
        </form>
        <footer>
            <div class="footer_h">
                <img id="logo_f" src="../img/logo.png">
                <h4>ОБУЧЕНИЕ&nbspРИЕЛТОРОВ</h4>
            </div>
            
            <div class="teleg">
                <a href="https://t.me/trainingrealtors_bot" title="ЧАТ-БОТ'Обучение Риэлторов'" target="_blank">
                    <img src="../img/Telegram.png"> ЧАТ-БОТ “обучение риелторов”
                </a>
            </div>
            <p> &copy;	2024 ”ОБУЧЕНИЕ РИЕЛТОРОВ” Made by @ar.alekso</p>
        </footer>
    </body>
</html>