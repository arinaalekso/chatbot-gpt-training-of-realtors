from langchain.chat_models import GigaChat
from langchain.prompts.chat import (
    ChatPromptTemplate,
    SystemMessagePromptTemplate,
    AIMessagePromptTemplate,
    HumanMessagePromptTemplate,
)
from langchain.chains import ConversationChain
from langchain.memory import ConversationBufferMemory
from langchain.schema import AIMessage, HumanMessage, SystemMessage
import telebot;
import button;
import ai;
import command;
from telebot import types
from config import host,user,password,db_name
import psycopg2
import requests
from time import sleep
import datetime

chat = GigaChat(credentials="YzU5ZTYwNDMtZjk3ZS00NzgzLTg1MDAtODkxYWFkMzM5ZmRmOmM3NTFjZjJjLTFkMjktNDM0OC04NDQ1LWIxOTlmZDY1MTg1Zg==", verify_ssl_certs=False)
bot = telebot.TeleBot('6944194637:AAG6ChQrrC_Jkt6w7LTEOE_3SsiwOCp3ixI');

user_conversations_sale = {}
template_sale = '''Ты хочешь продать квартиру.Ты не боишься потерять право собственности. Но у тебя есть возражения продавать квартиру при помощи риэлтора, то есть это может быть отказ от работы с агентствами недвижимости или сотрудничество с другим агентством, также отказ от оплаты комиссии. Тебе не нужно встречаться с риелтором, достаточно уведомить риелтора, что ты согласен работать с ним.
                \n\nТекущий разговор:\n{history}\nHuman: {input}\nAI:'''
conversation_sale = ConversationChain(llm=chat,verbose=True, memory=ConversationBufferMemory())
conversation_sale.prompt.template = template_sale

start_time = datetime.datetime.now()

def points(emp_point):
    emp_point=emp_point+1
    bot.send_message(f"Ваши баллы: {emp_point}")

try:
    connection=psycopg2.connect(
        host=host,
        user=user,
        password=password,
        database=db_name
    )

    def info_student(message):
        username=message.from_user.username;
        id_telemp=message.from_user.id;
        id_emp=0;
        surname="";
        name="";
        patronymic="";
        post="";
        city="";
        emp_point=int(0);
        with connection.cursor() as cursor:
            cursor.execute(
                f"SELECT * FROM info_tele_emp WHERE telegram_login='{username}' OR chat_id = '{id_telemp}' ;"
            )
            list_info_tele_emp=cursor.fetchall()
            for result in list_info_tele_emp:
                id_emp=result[1];
            print(id_emp)
            cursor.execute(
                f"SELECT * FROM employees WHERE id='{id_emp}';"
            )
            list_info_emp=cursor.fetchall()
            for result in list_info_emp:
                surname=result[1]
                name=result[2]
                patronymic=result[3]
                post=result[10]
                dep=result[9]
            cursor.execute(
                f"SELECT city FROM department WHERE id='{dep}';"
            )    
            list_info_dep=cursor.fetchall()
            for result in list_info_dep:
                city=result[0]
            cursor.execute(
                f"SELECT * FROM pointstraining WHERE id_emp='{id_emp}'"
                )
            list_emp_point=cursor.fetchall()
            for result in list_emp_point:
                emp_point=int(result[2])                                                                 
            p=emp_point
            bot.send_message(message.chat.id,f'Информация: \nФ.И.О.: {surname} {name} {patronymic}\nПост: {post}\nОтделение: город {city}\nОбщее количество баллов:{p}')


    @bot.message_handler(commands=['start'])
    def start(message):
        with connection.cursor() as cursor:
            try:
                cursor.execute(
                    f"SELECT id_emp FROM info_tele_emp WHERE telegram_login='{message.from_user.username}' OR chat_id = '{message.from_user.id}' ;"
                    )
                my_id = cursor.fetchone()[0]
                print(my_id)
                cursor.execute(
                    f"SELECT chat_id,telegram_login FROM info_tele_emp WHERE id_emp='{my_id}';"
                    )
                list_info_tl=cursor.fetchall()
                for result_tl in list_info_tl:
                    my_chat_id=result_tl[0]
                    my_telegram_login=result_tl[1]
                print(result_tl[0])
                if my_chat_id== message.from_user.id or my_telegram_login== message.from_user.username:
                    bot.send_message(message.chat.id,'Привет')
                    button.menu(message)
                    if my_chat_id is None:
                        cursor.execute(
                        f"UPDATE info_tele_emp SET chat_id ='{message.from_user.id}' WHERE id_emp='{my_id}';"
                        )
                        connection.commit()
            except:
                bot.send_message(message.chat.id, 'Вы не зарегистрироваться в системе. Обратитесь в учебную часть.');
    
    @bot.message_handler(commands=['main'])
    def message_reply(message):
        with connection.cursor() as cursor:
            try:
                cursor.execute(
                    f"SELECT id_emp FROM info_tele_emp WHERE telegram_login='{message.from_user.username}' OR chat_id = '{message.from_user.id}' ;"
                    )
                my_id = cursor.fetchone()[0]
                print(my_id)
                cursor.execute(
                    f"SELECT chat_id,telegram_login FROM info_tele_emp WHERE id_emp='{my_id}';"
                    )
                print(message.from_user.id)
                my_chat_id = cursor.fetchone()[0]
                print(my_chat_id)
                if my_chat_id  == message.from_user.id:
                    button.menu(message)
            except:
                bot.send_message(message.chat.id, 'Вы не зарегистрироваться в системе. Обратитесь в учебную часть.');
    @bot.message_handler(content_types=['audio','video','document','photo','sticker','voice','location','contact'])
    def not_text(message):
        bot.send_message(message.chat.id, 'Я работаю только с текстовыми сообщениями!')    
    @bot.message_handler(commands=['help'])
    def message_reply(message):
        with connection.cursor() as cursor:
            try:
                cursor.execute(
                    f"SELECT id_emp FROM info_tele_emp WHERE telegram_login='{message.from_user.username}' OR chat_id = '{message.from_user.id}' ;"
                    )
                my_id = cursor.fetchone()[0]
                print(my_id)
                cursor.execute(
                    f"SELECT chat_id,telegram_login FROM info_tele_emp WHERE id_emp='{my_id}';"
                    )
                print(message.from_user.id)
                my_chat_id = cursor.fetchone()[0]
                print(my_chat_id)
                if my_chat_id  == message.from_user.id:
                    command.help(message)
            except:
                bot.send_message(message.chat.id, 'Вы не зарегистрироваться в системе. Обратитесь в учебную часть.');
    @bot.message_handler(commands=['site'])
    def message_reply(message):
        with connection.cursor() as cursor:
            try:
                cursor.execute(
                    f"SELECT id_emp FROM info_tele_emp WHERE telegram_login='{message.from_user.username}' OR chat_id = '{message.from_user.id}' ;"
                    )
                my_id = cursor.fetchone()[0]
                print(my_id)
                cursor.execute(
                    f"SELECT chat_id,telegram_login FROM info_tele_emp WHERE id_emp='{my_id}';"
                    )
                print(message.from_user.id)
                my_chat_id = cursor.fetchone()[0]
                print(my_chat_id)
                if my_chat_id  == message.from_user.id:
                    command.site(message)
            except:
                bot.send_message(message.chat.id, 'Вы не зарегистрироваться в системе. Обратитесь в учебную часть.');
    @bot.message_handler(commands=['info'])
    def message_reply(message):
        with connection.cursor() as cursor:
            try:
                cursor.execute(
                    f"SELECT id_emp FROM info_tele_emp WHERE telegram_login='{message.from_user.username}' OR chat_id = '{message.from_user.id}' ;"
                    )
                my_id = cursor.fetchone()[0]
                print(my_id)
                cursor.execute(
                    f"SELECT chat_id,telegram_login FROM info_tele_emp WHERE id_emp='{my_id}';"
                    )
                print(message.from_user.id)
                my_chat_id = cursor.fetchone()[0]
                print(my_chat_id)
                if my_chat_id  == message.from_user.id:
                    info_student(message)
            except:
                bot.send_message(message.chat.id, 'Вы не зарегистрироваться в системе. Обратитесь в учебную часть.');(message)    
    
    @bot.message_handler(content_types=['text'])
    def message_reply(message):
        if message.text=="Информация о студенте ℹ️":
            info_student(message)
        elif message.text=="Сайт 🌐":
            command.site(message)
        elif message.text=="Помощь 🆘":
            command.help(message)
        elif message.text=="Сфера деятельности 🌆":
            button.sphere(message)
        elif message.text=="Продажа":
            bot.send_message(message.chat.id,'Скоро начнется тестирование',reply_markup=button.start_study_buy()) 
        elif message.text=="Начать тестирование": 
            ai.sale(message)
            username=message.from_user.username;
            id_telemp=message.from_user.id;
            with connection.cursor() as cursor:
                cursor.execute(f"SELECT * FROM info_tele_emp WHERE telegram_login='{username}' OR chat_id = '{id_telemp}' ;")
                list_info_tele_emp=cursor.fetchall()
                for result in list_info_tele_emp:
                    id_emp=result[1];
            with connection.cursor() as cursor:
                date_now=datetime.datetime.now().strftime('%Y-%m-%d')
                timeob= datetime.time(0,0,0)
                cursor.execute(
                    f"INSERT INTO info_dialog (id_emp,date_mess,totaltime,status_dialog,count_mess) VALUES('{id_emp}','{date_now}','{timeob}','{'Начат'}','{0}')"
                )
                connection.commit()
            button.stop_study(message)
        elif message.text=="Стоп🛑":
            print (start_time)
            end_time = datetime.datetime.now()
            print(end_time)
            username=message.from_user.username;
            id_telemp=message.from_user.id;
            with connection.cursor() as cursor:
                cursor.execute(f"SELECT * FROM info_tele_emp WHERE telegram_login='{username}' OR chat_id = '{id_telemp}' ;")
                list_info_tele_emp=cursor.fetchall()
                for result in list_info_tele_emp:
                    id_emp=result[1];
                cursor.execute(
                    f"UPDATE pointstraining SET point=point+1  WHERE id_emp ='{id_emp}' ;"
                )
                connection.commit()
                date_now=datetime.datetime.now().strftime('%Y-%m-%d')
                cursor.execute(
                    f"INSERT INTO time_and_points(id_emp, points,times)VALUES('{id_emp}','{1}','{date_now}');"
                )
                connection.commit()
            with connection.cursor() as cursor:
                cursor.execute(
                    f"SELECT * FROM pointstraining WHERE id_emp='{id_emp}'"
                    )
                list_emp_point=cursor.fetchall()
                for result in list_emp_point:
                    points=int(result[2])  
            with connection.cursor() as cursor:
                cursor.execute(
                    f"SELECT id from info_dialog WHERE id_emp='{id_emp}' and status_dialog='Начат' ORDER BY id DESC LIMIT 1 ;"
                )
                id_infodialog = cursor.fetchone()[0]  
                execution_time = end_time - start_time 
                print(execution_time)
                cursor.execute(
                    f"SELECT count(id) from history_chat_bot where id_dialog = '{id_infodialog}'"
                )
                count_mes = cursor.fetchone()[0] 
                cursor.execute(
                    f"UPDATE info_dialog SET  totaltime='{execution_time}',status_dialog='{'Закончен'}',count_mess='{count_mes}'WHERE id ='{id_infodialog}';"
                )
                connection.commit()
            bot.send_message(message.chat.id,f'Общее количество баллов:{points}.Скоро начнется тестирование',reply_markup=button.start_study())  
        if message.text=="В главное меню📋":
            button.menu(message) 
        elif ("регист" in message.text) or ("Регист" in message.text):
            button.reg(message)
    bot.infinity_polling(none_stop=True, interval=0)
except Exception as _ex:
    print("[INFO] Error while working with PostgreSQL", _ex)
finally:
    if connection: 
        connection.close()
        print("[INFO] PostgreSQL connection closed") 