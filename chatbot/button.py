import telebot
from telebot import types

token='6944194637:AAG6ChQrrC_Jkt6w7LTEOE_3SsiwOCp3ixI'
bot=telebot.TeleBot(token)

def menu(message):
    markup_start=types.ReplyKeyboardMarkup(row_width=1, resize_keyboard=True)
    btn_info=types.KeyboardButton('Информация о студенте ℹ️')
    btn_help=types.KeyboardButton('Помощь 🆘')
    markup_start.row(btn_info,btn_help)
    btn_www=types.KeyboardButton('Сайт 🌐')
    btn3=types.KeyboardButton('Сфера деятельности 🌆')
    markup_start.row(btn_www,btn3)
    bot.send_message(message.chat.id,'Главное меню',reply_markup=markup_start)

def start_study():
    markup_startstudy=types.ReplyKeyboardMarkup(row_width=2, resize_keyboard=True)
    btn_startstudy=types.KeyboardButton('Начать тестирование')
    btn_mainmenu=types.KeyboardButton('В главное меню📋')
    markup_startstudy.row(btn_startstudy,btn_mainmenu)
    return markup_startstudy

def start_study_buy():
    markup_startstudy=types.ReplyKeyboardMarkup(row_width=2, resize_keyboard=True)
    btn_startstudy=types.KeyboardButton("Начать тестирование")
    btn_mainmenu=types.KeyboardButton('В главное меню📋')
    markup_startstudy.row(btn_startstudy,btn_mainmenu)
    return markup_startstudy

def stop_study(message):
    markup_stopstudy=types.ReplyKeyboardMarkup(row_width=1, resize_keyboard=True)
    btn_stopstudy=types.KeyboardButton('Стоп🛑') 
    markup_stopstudy.row(btn_stopstudy)
    bot.send_message(message.chat.id,"Начать тестирование",reply_markup=markup_stopstudy)

def btn_sphere(message):
    markup_sphere=types.InlineKeyboardMarkup()
    sale=types.InlineKeyboardButton(text="Продажа",callback_data='sale')
    rent=types.InlineKeyboardButton(text="Аренда",callback_data='rent')
    markup_sphere.add(sale,rent)
    bot.send_message(message.chat.id,"Выберите сферу деятельности.",reply_markup=markup_sphere)

def sphere(message):
    markup_sphere=types.ReplyKeyboardMarkup(row_width=1, resize_keyboard=True)
    #btn_sphere1=types.KeyboardButton('Покупка')
    btn_sphere2=types.KeyboardButton('Аренда')
    btn_sphere3=types.KeyboardButton('Продажа')
    markup_sphere.row(btn_sphere2,btn_sphere3)
    btn_back=types.KeyboardButton('В главное меню📋')
    markup_sphere.row(btn_back)
    bot.send_message(message.chat.id,'Выбери сферу деятельности',reply_markup=markup_sphere)

def reg(message):
    reg_keyboard=types.InlineKeyboardMarkup()
    url_reg=types.InlineKeyboardButton(text="Регистрация",url="https://t.me/trainingrealtors_bot")
    reg_keyboard.add(url_reg)
    bot.send_message(message.chat.id,"Вы можете зарегистрироваться на сайте или обратиться в учебную часть.",reply_markup=reg_keyboard)    
