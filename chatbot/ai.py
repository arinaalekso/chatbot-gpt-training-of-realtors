from langchain.chat_models import GigaChat
from langchain.prompts.chat import (
    ChatPromptTemplate,
    SystemMessagePromptTemplate,
    AIMessagePromptTemplate,
    HumanMessagePromptTemplate,
)
from langchain.chains import ConversationChain
from langchain.memory import ConversationBufferMemory
from langchain.schema import AIMessage, HumanMessage, SystemMessage
import telebot
from time import sleep
from config import host,user,password,db_name
import psycopg2
import datetime

chat = GigaChat(credentials="YzU5ZTYwNDMtZjk3ZS00NzgzLTg1MDAtODkxYWFkMzM5ZmRmOmM3NTFjZjJjLTFkMjktNDM0OC04NDQ1LWIxOTlmZDY1MTg1Zg==", model='GigaChat:latest',verify_ssl_certs=False)
bot = telebot.TeleBot('6944194637:AAG6ChQrrC_Jkt6w7LTEOE_3SsiwOCp3ixI');
template_1 = '''Ты хочешь сдать квартиру. Но у тебя есть возражения сдать квартиру при помощи риэлтора, то есть это может быть отказ от работы с агентствами недвижимости или сотрудничество с другим агентством, также отказ от оплаты комиссии. Тебе не нужно встречаться с риелтором, достаточно уведомить риелтора, что ты согласен работать с ним.
\n\nТекущий разговор:\n{history}\nHuman: {input}\nAI:'''

try:
    connection=psycopg2.connect(
        host=host,
        user=user,
        password=password,
        database=db_name
    )

    #count=0

    @bot.message_handler(content_types=['text'])
    def sale(message):
        with connection.cursor() as cursor:
            cursor.execute(
                f"SELECT id_emp FROM info_tele_emp WHERE telegram_login='{message.from_user.username}' OR chat_id = '{message.from_user.id}' ;"
                )
            my_id = cursor.fetchone()[0]
        with connection.cursor() as cursor:
            cursor.execute(
                f"SELECT id from info_dialog WHERE id_emp='{my_id}' and status_dialog='Начат' ORDER BY id DESC LIMIT 1 ;"
                )
            id_infodialog = cursor.fetchone()[0]    
        user_conversations_sale = {}
        template_sale = '''Ты хочешь продать квартиру.Ты не боишься потерять право собственности. Но у тебя есть возражения продавать квартиру при помощи риэлтора, то есть это может быть отказ от работы с агентствами недвижимости или сотрудничество с другим агентством, также отказ от оплаты комиссии. Тебе не нужно встречаться с риелтором, достаточно уведомить риелтора, что ты согласен работать с ним.
        \n\nТекущий разговор:\n{history}\nHuman: {input}\nAI:'''
        
        conversation = ConversationChain(llm=chat,verbose=True,memory=ConversationBufferMemory())
        conversation.prompt.template = template_sale
        user_id = message.chat.id
        if user_id not in user_conversations_sale:
            user_conversations_sale[user_id] = ConversationBufferMemory()
        conversation.memory = user_conversations_sale[user_id]
        response = conversation.predict(input=message.text)
        bot.send_message(user_id, conversation.memory.chat_memory.messages[-1].content)
        user_mess=message.text
        bot_mess=conversation.memory.chat_memory.messages[-1].content
        times = datetime.datetime.now().strftime('%Y-%m-%d  %H:%M:%S')
        with connection.cursor() as cursor1:
            cursor1.execute(
                f"INSERT INTO history_chat_bot (id_emp,time_mess,users,mess,id_dialog) VALUES('{my_id}','{times}','{message.from_user.username}','{user_mess}','{id_infodialog}')")
            connection.commit()
        with connection.cursor() as cursor2: 
            cursor2.execute(
                f"INSERT INTO history_chat_bot (id_emp,time_mess,users,mess,id_dialog) VALUES('{my_id}','{times}','{'Чат-бот'}','{bot_mess}','{id_infodialog}')"
            )
            connection.commit()
        #count= count+1;
        sleep(2)
    bot.polling(none_stop=True)

    #def count_mess():return count
    
except Exception as _ex:
    print("[INFO] Error while working with PostgreSQL", _ex)
finally:
    if connection:
        # cursor.close()
        connection.close()
        print("[INFO] PostgreSQL connection closed") 